from django.contrib import admin
from .models import Project, Task, TaskTime, User, TaskPause

# Register your models here.
admin.site.register(Project)
admin.site.register(Task)
admin.site.register(TaskTime)
admin.site.register(User)
admin.site.register(TaskPause)
