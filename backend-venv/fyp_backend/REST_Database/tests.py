from django.test import TestCase
from REST_Database.views import CreateTrackingEntry, SetStatus
from rest_framework.test import APIRequestFactory
from .models import Project, Task, TaskTime, User, TaskPause

# REST_Test tests the view functions for each endpoint
class REST_Test(TestCase):
    def setUp(self):
        proj = Project.objects.create(project_code='TEST-01')
        Task.objects.create(task_id = 1, project_code = proj)
        User.objects.create(email='sahil.ahmed@hotmail.co.uk')

    # Test: create a time tracking entry
    def test_post_time_data(self):
        factory = APIRequestFactory()
        request = factory.post('tracker/create', {  'task_id': 1,
                                                    'email': 'sahil.ahmed@hotmail.co.uk',
                                                    'start_utc' : 'Tue, 22 Nov 2011 06:00:00 GMT',
                                                    'end_utc': 'Tue, 22 Nov 2011 06:00:00 GMT'  }, format='json')
        CreateTrackingEntry(request)

    def test_malformed_post_time_data(self):
        factory = APIRequestFactory()
        request = factory.post('tracker/create', {  'task_id': -1,
                                                    'email': 'sahi.co.u',
                                                    'start_utc' : 'Tue, 22 Nov 2011 06:00:00 GMT',
                                                    'end_utc': 'Tue, 22 Nov 2011 06:00:00 GMT'  }, format='json')
        CreateTrackingEntry(request)

    # Test: change status of a task. cycles through all statuses once
    def test_set_status(self):
        statuses = {'wait', 'inprg', 'done'}

        task = Task.objects.get(task_id=1)
        for status in statuses:

            factory = APIRequestFactory()
            request = factory.post('set/status', {  'task_id': 1,
                                                    'status': status   }, format='json')
            SetStatus(request)
            task = Task.objects.get(task_id=1)
            # print("Expected status: [{exp}] Actual Status: [{act}]".format(exp=status, act=task.status))

    def test_malformed_set_status(self):
        statuses = {'wait', 'inprg', 'done'}

        for status in statuses:
            factory = APIRequestFactory()
            request = factory.post('set/status', {  'task_id': -1,
                                                    'status': status   }, format='json')
            SetStatus(request)
            task = Task.objects.get(task_id=1)

class Pythonanywhere(TestCase):
    def setUp(self):
        print('hello')
    def post_time_data(self):
        r = requests.post("http://sahil221.pythonanywhere.com:8000", data={ 'task_id': 1,
                                                                        'email': 'sahil.ahmed@hotmail.co.uk',
                                                                        'start_utc' : 'Tue, 22 Nov 2011 06:00:00 GMT',
                                                                        'end_utc': 'Tue, 22 Nov 2011 06:00:00 GMT'  })
        print(r)
