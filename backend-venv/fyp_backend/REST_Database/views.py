from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import json
from datetime import datetime, timezone
from .models import Task, User, TaskTime

# Create your views here.

@csrf_exempt
def CreateTrackingEntry(request):
    if request.method == 'POST':

        # Get values from POST Request
        body = json.loads(request.body)
        _task_id = str(body['task_id'])
        _email = str(body['email'])
        _start_utc = str(body['start_utc'])
        _end_utc = str(body['end_utc'])

        # Convert time format from javascript (UTC) to python datetime object with UTC timzeone.
        _start_utc = datetime.strptime(_start_utc, "%a, %d %b %Y %H:%M:%S %Z").replace(tzinfo=timezone.utc)
        _end_utc = datetime.strptime(_end_utc, "%a, %d %b %Y %H:%M:%S %Z").replace(tzinfo=timezone.utc)

        # Get objects based on POST values
        try:
            _task = Task.objects.get(task_id=_task_id)
            _user = User.objects.get(email=_email)
        except Task.DoesNotExist as e:
            return HttpResponse("Not found in database", content_type="text/plain", status=404)

        tracking_entry = TaskTime(task=_task, user=_user, start_utc=_start_utc, end_utc=_end_utc)
        tracking_entry.save()

        return HttpResponse("Created Tracking Entry", content_type="text/plain", status=201)
    else:
        return HttpResponse("Must be post request", content_type="text/plain", status=401)

@csrf_exempt
def SetStatus(request):
    if request.method == 'POST':

        # Get values from POST Request
        body = json.loads(request.body)
        _task_id = str(body['task_id'])
        _status = str(body['status'])

        try:
            task = Task.objects.get(task_id=_task_id)
            task.status=_status
            task.save()
        except Task.DoesNotExist as e:
            return HttpResponse("Task not found in databse", content_type="text/plain", status=404)

        return HttpResponse("Set task " + _task_id + " status to ", content_type="text/plain", status=201)
    else:
        return HttpResponse("Must be post request", content_type="text/plain", status=401)
