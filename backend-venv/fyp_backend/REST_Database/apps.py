from django.apps import AppConfig


class RestDatabaseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'REST_Database'
