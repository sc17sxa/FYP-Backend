from django.db import models

# Models that define the database structure

class Project(models.Model):
    project_code = models.CharField(max_length=30, primary_key=True)
    project_name = models.CharField(max_length=30)

    def __str__(self):
        return u'%s - %s' % (self.project_code, self.project_name)

class Task(models.Model):
    STATUS_CHOICE = [   ('wait', 'Waiting'),
                        ('inprg', 'In Progress'),
                        ('done', 'Done')    ]

    PRIORITY_CHOICE = [ ('high', 'High'),
                        ('med', 'Medium'),
                        ('low', 'Low')  ]

    TASKTYPE_CHOICE = [ ('mtg', 'Meeting'),
                        ('dev', 'Software Development') ]

    task_id = models.AutoField(primary_key=True)
    project_code = models.ForeignKey(Project, on_delete=models.CASCADE)
    task_name = models.CharField(max_length=50)
    status = models.CharField(max_length=5, choices=STATUS_CHOICE, default='wait')
    priority = models.CharField(max_length=4, choices=PRIORITY_CHOICE, default='med')
    task_type = models.CharField(max_length=6, choices=TASKTYPE_CHOICE, default='dev')

    def __str__(self):
        return u'Task %s - %s' % (self.task_id, self.task_name)

class User(models.Model):
    email = models.CharField(max_length=100, primary_key=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        return u'%s' % (self.name)

class TaskTime(models.Model):
    tasktime_id = models.AutoField(primary_key=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # TODO: possibly change request structure so that we make 2 requests to update start and stop
    #start_utc = models.DateTimeField(auto_now_add=True)
    start_utc = models.DateTimeField()
    end_utc = models.DateTimeField()

class TaskPause(models.Model):
    taskpause_id = models.AutoField(primary_key=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
