/* *
 * Please note IntentReflectorHandler, SessionEndedRequestHandler, FallbackIntentHandler are boiler plate provided by amazon.
 * */

const Alexa = require('ask-sdk-core');
const rest_util = require('./REST_utility.js')

var start_datetime;
var current_task = -1;

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle(handlerInput) {
            speakOutput = 'Welcome to time tracker, what would you like to do?';

            return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt()
            .getResponse();
    }
};

const TrackingIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'TrackingIntent';
    },
    handle(handlerInput) {
        var _trackingState = handlerInput.requestEnvelope.request.intent.slots.trackingState.value;
        var _taskID = handlerInput.requestEnvelope.request.intent.slots.taskID.value;
        const { accessToken } = handlerInput.requestEnvelope.context.System.user; // get auth token for accessing user email

        var speakOutput = '';

        // Ensure we have user email before posting //TODO: should validate user email in database
        var user_email = rest_util.getUserEmail(accessToken);
        if(user_email === false){
            return handlerInput.responseBuilder
                .speak("Failed to get user email. Please contact the developer.")
                .getResponse();
        }

        if(_trackingState === 'start'){
            // Ensure task id is set
            if(!_taskID){
                return handlerInput.responseBuilder
                    .speak("What task would you like to track?")
                    .reprompt("What task would you like to track?")
                    .addElicitSlotDirective('taskID')
                    .getResponse();
            }else{
                current_task = _taskID;
            }

            // Start the timer
            start_datetime = new Date();
            speakOutput = 'Started time tracking task' + current_task;

           // Update task status in database
            rest_util.postStatus(current_task, 'inprg')
        }
        else if(_trackingState === 'finish' || _trackingState === 'stop' ){
            // Ensure task is set before posting data
            if(current_task<1 || isNaN(current_task)){
                return handlerInput.responseBuilder
                    .speak("What task would you like to stop?")
                    .reprompt("What task would you like to stop?")
                    .addElicitSlotDirective('taskID')
                    .getResponse();
            }
            // Update task status in database
            rest_util.postStatus(current_task, 'wait')

            rest_util.postTimeData(current_task, user_email, start_datetime)
            speakOutput = 'Stopped time tracking task ' + current_task;
            current_task = -1;


        }
        else{
            speakOutput = 'Sorry I do not understand' + _trackingState;
        }

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};

// Confirmation intent is called when we 'select' task, to confirm if we want to track it
const ConfirmationIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'ConfirmationIntent'
            && !isNaN(current_task)
            && current_task>0;
    },
    handle(handlerInput) {
        var _confirmation = handlerInput.requestEnvelope.request.intent.slots.confirmation.value;
        var speakOutput;

        if (_confirmation === 'yes'){
            speakOutput = 'yh started time tracking task ' + current_task;
            start_datetime = new Date();
        }else if (_confirmation === 'no'){
            speakOutput = 'OK, I\'ll remember this task for later';
        }

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};

const SetActiveTaskIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SetActiveTaskIntent';
    },
    handle(handlerInput) {
        var _taskID = handlerInput.requestEnvelope.request.intent.slots.taskID.value;
        var speakOutput = '';

        current_task = _taskID;
        speakOutput = "Current task has been set to " + _taskID + '. Would you like to start tracking it now?';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt('Would you like to start tracking it now?')
            .getResponse();
    }
};


/* *
 * FallbackIntent triggers when a customer says something that doesn’t map to any intents in your skill
 * It must also be defined in the language model (if the locale supports it)
 * This handler can be safely added but will be ingnored in locales that do not support it yet
 * */
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'Sorry, I don\'t know about that. Please try again.';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

/* *
 * SessionEndedRequest notifies that a session was ended. This handler will be triggered when a currently open
 * session is closed for one of the following reasons: 1) The user says "exit" or "quit". 2) The user does not
 * respond or says something that does not match an intent defined in your voice model. 3) An error occurs
 * */

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`~~~~ Session ended: ${JSON.stringify(handlerInput.requestEnvelope)}`);
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse(); // notice we send an empty response
    }
};

/* *
 * The intent reflector is used for interaction model testing and debugging.
 * It will simply repeat the intent the user said. You can create custom handlers for your intents
 * by defining them above, then also adding them to the request handler chain below
 * */
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
/**
 * Generic error handling to capture any syntax or routing errors. If you receive an error
 * stating the request handler chain is not found, you have not implemented a handler for
 * the intent being invoked or included it in the skill builder below
 * */
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const speakOutput = 'Sorry, I had trouble doing what you asked. Please try again.';
        console.log(`~~~~ Error handled: ${JSON.stringify(error)}`);

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

/**
 * This handler acts as the entry point for your skill, routing all request and response
 * payloads to the handlers above. Make sure any new handlers or interceptors you've
 * defined are included below. The order matters - they're processed top to bottom
 * */
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        TrackingIntentHandler,
        SetActiveTaskIntentHandler,
        ConfirmationIntentHandler,
        FallbackIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler)
    .addErrorHandlers(
        ErrorHandler)
    .withCustomUserAgent('sample/hello-world/v1.2')
    .lambda();
