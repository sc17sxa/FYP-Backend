const http = require('http')
const https = require('https')

module.exports = {

    // Post time tracking data to database
    postTimeData: function (current_task, user_email, start_datetime) {
        var end_datetime = new Date();
        var data = JSON.stringify;

        // Setup correct data and options for Djano instance
        data = JSON.stringify({
                    "task_id": current_task,
                    "email": user_email,
                    "start_utc": start_datetime.toUTCString(),
                    "end_utc": end_datetime.toUTCString()
                })

        const options = {
          hostname: 'sahil221.pythonanywhere.com',
          path: '/tracker/create',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
          }
        }

        // Attempt to post data
        const req = http.request(options, res => {
          console.log(`statusCode: ${res.statusCode}`)

          res.on('data', d => {
            process.stdout.write(d)
          })
        })

        req.on('error', error => {
          console.error(error)
        })

        req.write(data)
        req.end()

    },

    // Get user email from Amazon API
    getUserEmail: function (token) {
        console.log("getUserData");
        var url = 'https://api.amazon.com/user/profile?access_token=' + token;

        https.get(url, (resp) => {
            let data = '';

            // A chunk of data has been received.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // Get email value from Json response
            resp.on('end', () => {
                user_email = JSON.parse(data).email;
            });

        }).on("error", (err) => {
            user_email = "";
            console.log("Error: " + err.message);
            return false;
        });

        return user_email;
    },

    // Change status of task
    postStatus: function (current_task, status) {
        var end_datetime = new Date();
        var data = JSON.stringify;

        // Setup correct data and options for Django instance
        data = JSON.stringify({
                    "task_id": current_task,
                    "status": status
                })

        const options = {
          hostname: 'sahil221.pythonanywhere.com',
          path: '/set/status',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
          }
        }

        // Attempt to post data
        const req = http.request(options, res => {
          console.log(`statusCode: ${res.statusCode}`)

          res.on('data', d => {
            process.stdout.write(d)
          })
        })

        req.on('error', error => {
          console.error(error)
        })

        req.write(data)
        req.end()

    }
};
